
import { createStore, compose, applyMiddleware, Middleware } from 'redux'
import { reducer } from './rootReducer'
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';


// @todo - move this to separate file
const apiMiddleware: Middleware = () => next => action => {
  next(action);
    return;
}

const enhancer = compose(
  applyMiddleware(apiMiddleware),
  composeWithDevTools()
);


export const store = createStore(reducer, enhancer);
