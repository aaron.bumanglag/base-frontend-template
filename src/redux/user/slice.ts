import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface UserState {
  loading: boolean;
  error: boolean;
  data: {}
}

const initialState = { loading: false , error: false, data: {}} as UserState

const user = createSlice({
  name: 'user',
  initialState,
  reducers: {
    get(state, action?: PayloadAction<any>) {
      state.data = {
        id: 123,
        name: 'Caleb',
        isCool: false
      }
    },
  },
})

// @note - dispatch these actions as onSuccess in the middleware 
export const { get } = user.actions
export default user.reducer