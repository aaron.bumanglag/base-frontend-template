import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import App from './App';
import THEME from './Themes/Theme';
import { ThemeProvider } from '@material-ui/core/styles';
import './styles.scss';

const mountNode = document.getElementById('app');
ReactDOM.render(
  <Provider store={store}>
      <ThemeProvider theme={THEME}>
        <App name="Govini" />
      </ThemeProvider>
  </Provider>,
  mountNode,
);
