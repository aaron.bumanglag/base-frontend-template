import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import { get } from './redux/user/slice';
import { getUser } from './redux/user/selectors';

interface AppProps {
  name: string;
}

const App: React.FC<AppProps> = ({ name }) => {
  const dispatch = useDispatch();
  const handleClick = () => dispatch(get({}));
  const user = useSelector(getUser);
  return (
    <>
      <h1>
        {name}
        {name}
      </h1>
      <div>
        {JSON.stringify(user)}
        <div />
      </div>
      <Button onClick={handleClick} variant="containedPrimary">
        this is a material button
      </Button>
    </>
  );
};

export default App;
