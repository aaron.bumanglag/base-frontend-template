import { createTheme } from '@material-ui/core/styles';

import {
  ORANGE_6, LUST, GRAY_2, GRAY_10, GRAY_5, GRAY_9, BLUE_6,
  ORANGE_6_HOVER, ORANGE_6_DISABLED, GRAY_7, GRAY_3, GRAY_6
} from '../Constants';

const THEME = createTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
      main: ORANGE_6,
      contrastText: GRAY_2,
    },
    secondary: {
      main: GRAY_2,
      contrastText: GRAY_10,
    },
    // This is shown on various MUI components when in an error state. For
    // example text areas will have a border of this color.
    error: {
      main: LUST,
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
  typography: {
    fontFamily: '"Lato"',
  },
  // Global CSS overrides
  overrides: {
    MuiInputLabel: {
      outlined: {
        backgroundColor: "white",
        paddingLeft: 2,
        paddingRight: 2
      }
    },
    MuiIconButton: {
      root:{
        '&:hover': {
          backgroundColor: 'unset'
        },
        "&$disabled": {
          opacity: "30%"
        }
      }
    },
    MuiButton: {
      contained: {
        boxShadow: "none",
      },
      containedSecondary: {
        border: "1px solid",
        borderColor: GRAY_6,
        color: GRAY_9,
        backgroundColor: "white",
        letterSpacing: "0.3px",
        height: "32px",
        textTransform: "none",

        '&:hover': {
          backgroundColor: "rgba(0,0,0,0.04)",
          boxShadow: "none",
        },
        "&$disabled": {
          backgroundColor: "rgba(255,255,255,0.35)",
          borderColor: "rgba(128,137,142,0.35)",
          color: "rgba(26,42,51,0.35)",
        },
      },
      containedPrimary: {
        letterSpacing: "0.3px",
        height: "32px",
        minWidth: "88px",
        textTransform: "uppercase",

        '&:hover': {
          backgroundColor: ORANGE_6_HOVER,
          boxShadow: "none",
        },
        "&$disabled": {
          backgroundColor: ORANGE_6_DISABLED,
          color: "rgba(255, 255, 255, 0.4)",
        },
      },
      textPrimary: {
        "&$disabled": {
          backgroundColor: ORANGE_6_DISABLED,
          color: "#FFFFFF",
        },
      },
      textSecondary: {
        color: GRAY_9,
        backgroundColor: "white",
        letterSpacing: "0.3px",
        height: "32px",
        textTransform: "none",

        '&:hover': {
          backgroundColor: "rgba(0,0,0,0.04)",
          boxShadow: "none",
        },
        "&$disabled": {
          backgroundColor: "rgba(255,255,255,0.35)",
          borderColor: "rgba(128,137,142,0.35)",
          color: "rgba(26,42,51,0.35)",
        },
      },
    },
    MuiSelect: {
      root: {
        border: "1px solid",
        borderColor: GRAY_5,
        borderRadius: "4px !important",
        backgroundColor: "white",
        paddingLeft: "8px",

        '&:hover': {
          border: "1px solid",
          borderColor: GRAY_7,
        }
      },
    },
    MuiChip: {
      deleteIcon: {
        color: "white",

        '&:hover': {
          color: GRAY_3,
        }
      }
    },
    MuiTooltip: {
      tooltip: {
        backgroundColor: GRAY_9,
        color: "#FFFFFF",
      },
      arrow: {
        color: GRAY_9,
      }
    },
    MuiFormLabel: {
      root: {
        '&$focused': {
          color: BLUE_6,
        },
      },
    },
    MuiOutlinedInput: {
      root: {
        '&$focused $notchedOutline': {
          borderColor: BLUE_6,
          borderWidth: '1px',
        },
      }
    },
  },
});

export default THEME;
